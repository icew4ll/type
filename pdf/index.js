import React from 'react';
import ReactPDF from '@react-pdf/renderer';
import { Pdf } from './pdf';

ReactPDF.render(<Pdf />, `${__dirname}/example.pdf`);
