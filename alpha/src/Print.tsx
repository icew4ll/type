import React, { useRef } from 'react';
import ReactToPrint, { useReactToPrint } from 'react-to-print';
import { ActionButton } from './ActionButton';
import { ComponentToPrint } from './ComponentToPrint';

export const Print = function Print(): JSX.Element {
  const componentRef = useRef<HTMLDivElement>(null);

  return (
    <div>
      <ReactToPrint
        trigger={() => <button type="button">Generar</button>}
        content={() => componentRef.current}
      />
      <div ref={componentRef}>
        <div>
          test
        </div>
      </div>
    </div>
  );
};
