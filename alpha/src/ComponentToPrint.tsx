import React from "react";

type Props = {
  componentToPrint: React.MutableRefObject<null>;
};

export const ComponentToPrint = function ComponentToPrint(props: Props): JSX.Element {
  const { componentToPrint } = props;
  return (
    <div ref={(el) => (componentToPrint.current = el as HTMLDivElement)}>
      test
    </div>
  );
};

// export const ComponentToPrint = function ComponentToPrint(props: Props): JSX.Element {
//   const { componentToPrint } = props;
//   return (
//     <div ref={(el) => (componentToPrint.current = el)}>
//       <table>
//         <thead>
//           <th>column 1</th>
//           <th>column 2</th>
//           <th>column 3</th>
//         </thead>
//         <tbody>
//           <tr>
//             <td>data 1</td>
//             <td>data 2</td>
//             <td>data 3</td>
//           </tr>
//           <tr>
//             <td>data 1</td>
//             <td>data 2</td>
//             <td>data 3</td>
//           </tr>
//           <tr>
//             <td>data 1</td>
//             <td>data 2</td>
//             <td>data 3</td>
//           </tr>
//         </tbody>
//       </table>
//     </div>
//   );
// };
