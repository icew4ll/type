// TODO
// https://github.com/microsoft/TypeScript/issues/41882
// In React 17 you no longer need to import react when writing JSX
// import React from 'react';
import {
  Page, Text, View, Document, StyleSheet,
} from '@react-pdf/renderer';

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4',
  },
  section: {
    color: 'green',
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
});

// Create Document Component
export const Pdf = function Pdf(): JSX.Element {
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={styles.section}>
          <Text>Section #1</Text>
        </View>
        <View style={styles.section}>
          <Text>Section #2</Text>
        </View>
      </Page>
    </Document>
  );
};
