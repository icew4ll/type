// import ReactPDF from '@react-pdf/renderer';
import { PDFViewer } from '@react-pdf/renderer';
import logo from './logo.svg';
import './App.css';
import { Pdf } from './Pdf';
import { Print } from './Print';

// ReactPDF.render(<Pdf />, `${__dirname}/example.pdf`);

const App = function App(): JSX.Element {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit
          {' '}
          <code>src/App.tsx</code>
          {' '}
          and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Print />
      <PDFViewer>
        <Pdf />
      </PDFViewer>
    </div>
  );
};

export default App;
